﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShapeGenerator
{
    /// <summary>
    /// Generates a random shape with input and output
    /// </summary>
    /// <param name="numInput"></param>
    /// <param name="numSolid"></param>
    /// <param name="numOutput"></param>
    /// <returns></returns>    
    public static SpellItem.Segment[,] GenerateShape(int numInput, int numSolid, int numOutput)
    {
        List<int[]> solidPositions = new List<int[]>();
        int shapeSize = numSolid + numOutput;
        int arraySize = Mathf.CeilToInt(shapeSize / 2) + 1;
        SpellItem.Segment[,] shape = new SpellItem.Segment[arraySize, arraySize];

        // Init shape to empty
        for (int i = 0; i < arraySize; i++)
        {
            for (int j = 0; j < arraySize; j++)
            {
                shape[i, j] = SpellItem.Segment.Empty;
            }
        }

        // Init start solid position
        int[] randPos = new int[] { Mathf.RoundToInt(Random.Range(0, arraySize - 1)), Mathf.RoundToInt(Random.Range(0, arraySize - 1)) };
        shape[randPos[0], randPos[1]] = SpellItem.Segment.Solid;
        solidPositions.Add(randPos);

        // generate random shape of solid and add to shape
        while (solidPositions.Count < shapeSize)
        {
            for (int i = 0; i < arraySize; i++)
            {
                for (int j = 0; j < arraySize; j++)
                {
                    if (HasNeighbour(i, j, shape) && WeightedRandom(i, j, arraySize) && solidPositions.Count != shapeSize)
                    {
                        shape[i, j] = SpellItem.Segment.Solid;
                        solidPositions.Add(new int[] { i, j });
                    }
                }
            }
        }

        while (numOutput > 0)
        {
            int randSolid = Mathf.RoundToInt(Random.Range(0, solidPositions.Count - 1));
            int[] solidPos = solidPositions[randSolid];
            int[] randPosition = GetEmptyNeighbour(solidPos[0], solidPos[1], shape);
            if (randPosition != null)
            {
                if (solidPos[0] > randPosition[0])
                {
                    shape[solidPos[0], solidPos[1]] = SpellItem.Segment.OutputLeft;
                }
                else if (solidPos[0] < randPosition[0])
                {
                    shape[solidPos[0], solidPos[1]] = SpellItem.Segment.OutputRight;
                }
                else if (solidPos[1] > randPosition[1])
                {
                    shape[solidPos[0], solidPos[1]] = SpellItem.Segment.OutputDown;
                }
                else if (solidPos[1] < randPosition[1])
                {
                    shape[solidPos[0], solidPos[1]] = SpellItem.Segment.OutputUp;
                }
                numOutput--;
            }
            else
            {
                randSolid = (randSolid + 1) % arraySize;
            }
        }

        SpellItem.Segment[,] finalShape = new SpellItem.Segment[arraySize + 2, arraySize + 2];
        // Init shape to empty
        for (int i = 0; i < arraySize + 2; i++)
        {
            for (int j = 0; j < arraySize + 2; j++)
            {
                if (i - 1 > -1 && i - 1 < (arraySize) && j - 1 > -1 && j - 1 < (arraySize))
                {
                    finalShape[i, j] = shape[i - 1, j - 1];
                }
                else
                {
                    finalShape[i, j] = SpellItem.Segment.Empty;
                }
            }
        }

        while (numInput > 0)
        {
            int randSolid = Mathf.RoundToInt(Random.Range(0, solidPositions.Count));
            int[] solidPos = new int[] { solidPositions[randSolid][0] + 1, solidPositions[randSolid][1] + 1 };
            int[] randPosition = GetEmptyNeighbour(solidPos[0], solidPos[1], finalShape);
            if (randPosition != null)
            {
                if (solidPos[0] > randPosition[0] && finalShape[solidPos[0], solidPos[1]] != SpellItem.Segment.OutputLeft)
                {
                    finalShape[randPosition[0], randPosition[1]] = SpellItem.Segment.InputRight;
                }
                else if (solidPos[0] < randPosition[0] && finalShape[solidPos[0], solidPos[1]] != SpellItem.Segment.OutputRight)
                {
                    finalShape[randPosition[0], randPosition[1]] = SpellItem.Segment.InputLeft;
                }
                else if (solidPos[1] > randPosition[1] && finalShape[solidPos[0], solidPos[1]] != SpellItem.Segment.OutputDown)
                {
                    finalShape[randPosition[0], randPosition[1]] = SpellItem.Segment.InputUp;
                }
                else if (solidPos[1] < randPosition[1] && finalShape[solidPos[0], solidPos[1]] != SpellItem.Segment.OutputUp)
                {
                    finalShape[randPosition[0], randPosition[1]] = SpellItem.Segment.InputDown;
                }
                numInput--;
            }
        }

        return finalShape;
    }

    /// <summary>
    /// returns true if the position has a neighbouring filled cell
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="shape"></param>
    /// <returns></returns>
    public static bool HasNeighbour(int x, int y, SpellItem.Segment[,] shape)
    {
        if ((x + 1) < shape.GetLength(0) && shape[x + 1, y] == SpellItem.Segment.Solid ||
            (x - 1) > -1 && shape[x - 1, y] == SpellItem.Segment.Solid ||
            (y + 1) < shape.GetLength(0) && shape[x, y + 1] == SpellItem.Segment.Solid ||
            (y - 1) > -1 && shape[x, y - 1] == SpellItem.Segment.Solid)
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// Gets a random value where the further away from the center the less likely to be picked
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="size"></param>
    /// <returns></returns>
    public static bool WeightedRandom(int x, int y, int size)
    {
        Vector2 point = new Vector2(x, y);
        Vector2 midPoint = new Vector2(size / 2, size / 2);
        float randomVal = Random.Range(0, (size / 2) + 0.5f);
        float weightedRange = Vector2.Distance(point, midPoint);
        return randomVal > weightedRange;
    }

    /// <summary>
    /// Gets an empty neighbouring cell
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="shape"></param>
    /// <returns></returns>
    public static int[] GetEmptyNeighbour(int x, int y, SpellItem.Segment[,] shape)
    {
        List<int[]> emptySpaces = new List<int[]>();
        if ((x + 1) < shape.GetLength(0) && shape[x + 1, y] == SpellItem.Segment.Empty || x + 1 == shape.GetLength(0))
            emptySpaces.Add(new int[] { x + 1, y });
        if ((x - 1) > -1 && shape[x - 1, y] == SpellItem.Segment.Empty || x - 1 == -1)
            emptySpaces.Add(new int[] { x - 1, y });
        if ((y + 1) < shape.GetLength(0) && shape[x, y + 1] == SpellItem.Segment.Empty || y + 1 == shape.GetLength(0))
            emptySpaces.Add(new int[] { x, y + 1 });
        if ((y - 1) > -1 && shape[x, y - 1] == SpellItem.Segment.Empty || y - 1 == -1)
            emptySpaces.Add(new int[] { x, y - 1 });

        if (emptySpaces.Count > 0)
        {
            return emptySpaces[Mathf.RoundToInt(Random.Range(0, emptySpaces.Count))];
        }

        return null;
    }

    /// <summary>
    /// Returns an image string in binary
    /// 000
    /// 0 0
    /// 000
    /// is the default
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="shape"></param>
    /// <returns></returns>
    public static Sprite GetCellImage(int x, int y, SpellItem.Segment[,] shape)
    {
        string imageString = "";
        for (int i = -1; i < 1; i++)
        {
            for (int j = -1; j < 1; j++)
            {
                if ((i != 0 && j != 0 && x - i >= 0 && y - j >= 0 && x + i < shape.GetLength(0) && y + j < shape.GetLength(1)) && shape[x, y] != SpellItem.Segment.Empty)
                {
                    imageString = imageString + "1";
                }
                else
                {
                    imageString = imageString + "0";
                }
            }
        }
        return Resources.Load<Sprite>("Images/test"); ;
    }
}
