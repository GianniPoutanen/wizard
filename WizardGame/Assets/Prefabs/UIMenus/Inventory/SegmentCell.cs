﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SegmentCell : MonoBehaviour
{
    public SpellItem.Segment Segment { get; set; }
    public SpellItem ParentItem { get; set; }


    /// <summary>
    /// Returns the positional offset of the slot under the current segment
    /// </summary>
    /// <returns></returns>
    public Vector2 GetSlotPositionOffset()
    {
        List<RaycastResult> raycastResults = RaycastPoint();
        if (raycastResults.Count > 0)
        {
            foreach (RaycastResult raycastResult in raycastResults)
            {
                Slot slot = raycastResult.gameObject.GetComponent<Slot>();
                if (slot != null)
                {
                    return slot.transform.position - this.transform.position;
                }
            }
        }
        return Vector2.negativeInfinity;
    }

    /// <summary>
    /// Checks if the slot underneath this is free
    /// </summary>
    public bool CheckSlotFree()
    {
        List<RaycastResult> raycastResults = RaycastPoint();
        if (raycastResults.Count > 0)
        {
            foreach (RaycastResult raycastResult in raycastResults)
            {
                Slot slot = raycastResult.gameObject.GetComponent<Slot>();
                if (IsInput())
                {
                    SegmentCell output = raycastResult.gameObject.GetComponent<SegmentCell>();
                    if (output != null)
                    {
                        return CheckInputMatch(output);
                    }
                }
                else if (slot != null && !slot.IsInventory)
                {
                    {
                        slot.Flash = !slot.Flash;
                        return !slot.IsOccupied;
                    }
                }
            }
        }
        return false;
    }

    /// <summary>
    /// returns true if the input matches the output segment
    /// </summary>
    /// <param name="output"></param>
    /// <returns></returns>
    private bool CheckInputMatch(SegmentCell output)
    {
        int inputRotation = (int)((Mathf.RoundToInt(this.transform.rotation.eulerAngles.z / 90) * 90) % 360);
        int outputRotation = (int)((Mathf.RoundToInt(output.transform.rotation.eulerAngles.z / 90) * 90) % 360);

        int objRotation = (int)(Mathf.FloorToInt(output.transform.rotation.eulerAngles.z));

        if (inputRotation == objRotation)
        {
            return true;
        }
        return false;
    }

    /// <summary>
    ///  Gets all raycast results of objects under mouse
    /// </summary>
    private List<RaycastResult> RaycastPoint()
    {
        PointerEventData pointerData = new PointerEventData(EventSystem.current)
        {
            pointerId = -1,
        };
        pointerData.position = this.transform.position;
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointerData, results);
        return results;
    }

    /// <summary>
    /// returns true if this segment is of type input
    /// </summary>
    /// <returns></returns>
    public bool IsInput()
    {
        if (Segment == SpellItem.Segment.InputDown || Segment == SpellItem.Segment.InputLeft || Segment == SpellItem.Segment.InputRight || Segment == SpellItem.Segment.InputUp)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Gets the rotation relative to the segment type up being 0
    /// </summary>
    /// <returns></returns>
    private float RelativeRotation()
    {
        if (Segment == SpellItem.Segment.InputUp || Segment == SpellItem.Segment.OutputUp)
        {
            return 90;
        }
        else if (Segment == SpellItem.Segment.InputDown || Segment == SpellItem.Segment.OutputDown)
        {
            return 180;
        }
        else if (Segment == SpellItem.Segment.InputLeft || Segment == SpellItem.Segment.OutputLeft)
        {
            return 270;
        }
        else
        {
            return 0;
        }
    }

    /// <summary>
    /// Gets the slot underneath this segmnet - used when dropping spell items
    /// </summary>
    /// <returns></returns>
    public Slot GetSlotUnder()
    {
        List<RaycastResult> raycastResults = RaycastPoint();
        if (raycastResults.Count > 0)
        {
            foreach (RaycastResult raycastResult in raycastResults)
            {
                Slot slot = raycastResult.gameObject.GetComponent<Slot>();
                if (slot != null && !slot.IsInventory && !slot.IsOccupied)
                {
                    return slot;
                }
            }
        }
        return null;
    }
    /// <summary>
    /// returns the output segment under this segment - used when dropping input segments
    /// </summary>
    /// <returns></returns>
    public SegmentCell GetOutputUnder()
    {
        List<RaycastResult> raycastResults = RaycastPoint();
        if (raycastResults.Count > 0)
        {
            foreach (RaycastResult raycastResult in raycastResults)
            {
                SegmentCell output = raycastResult.gameObject.GetComponent<SegmentCell>();
                if (output != null)
                {
                    return output;
                }
            }
        }
        return null;
    }
}
