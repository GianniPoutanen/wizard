﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Item : MonoBehaviour, IEndDragHandler, IDragHandler, IBeginDragHandler
{
    [Header("Position of item variables")]
    public Vector2 anchorPoint;
    public Vector2 currentPosition;
    public Vector2 dragPosition;
    private bool rotating;
    public bool movementOveride;

    [Header("Rotation factor variables")]
    public float rotationFactor;
    private float rotationSpeed;
    public float rotationModifier;
    public float baseRotation;

    private bool shrunk;
    public bool Dragging { get; set; }
    public List<Slot> OccupiedSlots { get; set; } = new List<Slot>();
    public RectTransform RectTransform { get; set; }

    // Start is called on object Start
    private void Start()
    {
        dragPosition = this.transform.position;
        RectTransform = this.GetComponent<RectTransform>();
    }

    // Update is called once per frame
    public virtual void Update()
    {
        if (!movementOveride)
        {
            DraggingHandler();
        }
    }

    /// <summary>
    /// Called in Update, handles drag drop events
    /// </summary>
    public virtual void DraggingHandler()
    {
        if (Dragging)
        {
            this.transform.position = Vector2.MoveTowards(this.transform.position, dragPosition, 1.0f + (Vector2.Distance(this.transform.position, dragPosition) / 5));
            if (!rotating)
            {
                if (Input.GetAxis("Mouse ScrollWheel") > 0f) // forward
                {
                    baseRotation += 90;
                    Debug.Log("Scroll up");
                }
                else if (Input.GetAxis("Mouse ScrollWheel") < 0f) // backwards
                {
                    baseRotation -= 90;
                    Debug.Log("Scroll down");
                }
                else
                {
                    DragRotationHandler();
                }
            }
        }
        else
        {
            this.transform.rotation = Quaternion.Euler(0, 0, baseRotation);
            (this.transform as RectTransform).anchoredPosition = Vector2.MoveTowards((this.transform as RectTransform).anchoredPosition, anchorPoint, 1.0f + 
                    (Vector2.Distance((this.transform as RectTransform).anchoredPosition, anchorPoint) / 5)); ;
        }
    }

    /// <summary>
    /// Drag effect of items in inventory
    /// </summary>
    public void DragRotationHandler()
    {
        float distanceModifier = Vector2.Distance(this.transform.position, dragPosition);
        if (distanceModifier > 30)
            distanceModifier = 30;
        rotationSpeed = 0.1f * distanceModifier + 15f;
        rotationModifier = 0;

        if (dragPosition.x < this.transform.position.x)
        {
            rotationModifier = ((distanceModifier * rotationFactor) + baseRotation) % 360;
        }
        else if ((dragPosition.x > this.transform.position.x))
        {
            rotationModifier = (-(distanceModifier * rotationFactor) + baseRotation) % 360;
        }
        else
        {
            rotationModifier = baseRotation;
        }
        this.transform.rotation = Quaternion.RotateTowards(this.transform.rotation, Quaternion.Euler(0, 0, (rotationModifier)), rotationSpeed);
    }

    #region Dragging Functions
    public virtual void OnBeginDrag(PointerEventData eventData)
    {
        RemoveFromSlots();
        Shrink = false;
        this.GetComponent<Image>().raycastTarget = false;
        Debug.Log("start drag");
        this.transform.parent = GameObject.Find("Inventory").transform;
        Dragging = true;
    }

    public virtual void OnDrag(PointerEventData eventData)
    {
        Debug.Log("dragging");
        dragPosition = Input.mousePosition;
    }

    public virtual void OnEndDrag(PointerEventData eventData)
    {
        this.GetComponent<Image>().raycastTarget = true;
        Debug.Log("end drag");
        Dragging = false;
        (this.transform as RectTransform).anchoredPosition = Vector3.zero;
        if (this.transform.parent.gameObject == GameObject.Find("Inventory"))
        {
            GameObject.Find("Inventory").GetComponent<InventoryManager>().AddToInventory(this);
        }
    }
    #endregion Dragging Functions

    /// <summary>
    ///  Gets all raycast results of objects under mouse
    /// </summary>
    public List<RaycastResult> RaycastMouse()
    {
        PointerEventData pointerData = new PointerEventData(EventSystem.current)
        {
            pointerId = -1,
        };
        pointerData.position = Input.mousePosition;
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointerData, results);
        return results;
    }

    /// <summary>
    /// Shrink property call
    /// </summary>
    public bool Shrink
    {
        get { return shrunk; }
        set
        {
            if (value == true && shrunk == false)
            {
                ShrinkForInventory();
            }
            if (value == false && shrunk == true)
            {
                this.transform.localScale = new Vector3(1, 1, 1);
            }
            shrunk = value;
        }
    }

    /// <summary>
    /// shrinks the objects size for inventory
    /// </summary>
    public virtual void ShrinkForInventory()
    {
        this.transform.localScale = this.transform.localScale * 0.8f;
    }

    /// <summary>
    /// Removes this items reference from the slots
    /// </summary>
    public virtual void RemoveFromSlots()
    {
        foreach (Slot slot in OccupiedSlots)
        {
            slot.IsOccupied = false;
        }
        OccupiedSlots.Clear();
    }
}
