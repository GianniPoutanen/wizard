﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SpellItem : Item
{
    private bool shrunk;
    public enum Segment
    {
        Empty,
        Solid,
        InputRight,
        InputLeft,
        InputUp,
        InputDown,
        OutputRight,
        OutputLeft,
        OutputUp,
        OutputDown
    }
    [Header("Shape in inventory")]
    // 2 is solid position, 1 is in position, 3 is out position
    public Segment[,] shape = new Segment[,] { { Segment.Empty, Segment.Empty, Segment.Empty },
                                               { Segment.InputUp, Segment.OutputRight, Segment.Empty},
                                               { Segment.Empty, Segment.Empty, Segment.Empty } };

    [Header("Spell building variables")]
    public SpellSegment spellSegment = new SpellSegment();
    public GameObject inputOutputImage;
    public GameObject solidImage;
    private Dictionary<GameObject, Segment> inputObjectPairs = new Dictionary<GameObject, Segment>();
    private List<GameObject> outputObjects = new List<GameObject>();
    private List<GameObject> solidBlockObjects = new List<GameObject>();
    private bool slotLockIn = false;
    private float rotationLock;
    public List<SpellItem> followingItems;
    public SpellSegment SpellSegment { get; set; }
    // Start is called before the first frame update
    void Start()
    {
        CreateSpellSegmentPiece();
        rotationLock = rotationFactor;
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        //HandleGridScrollRotation();
        ItemSlotInHandler();
    }

    private void ItemSlotInHandler()
    {
        if (CanDrop())
        {
            Vector2 pos = ((Vector2)this.transform.position + (this.transform.GetChild(0).GetComponent<SegmentCell>().GetSlotPositionOffset()));
            Debug.Log(Vector2.Distance(pos, Input.mousePosition));
            if (/*!slotLockIn &&*/ Vector2.Distance(pos, Input.mousePosition) < 15)
            {
                slotLockIn = true;
                rotationFactor = 0;
                dragPosition = pos;
            }
            else
            {
                dragPosition = Input.mousePosition;
            }
        }
        else
        {
            dragPosition = Input.mousePosition;
            rotationFactor = rotationLock;
            slotLockIn = false;
        }
    }

    /// <summary>
    /// handles the rotation affects when slotting in
    /// </summary>
    private void HandleGridScrollRotation()
    {
        //unlock rotation and stotting if scroll wheel
        if (Input.GetAxis("Mouse ScrollWheel") != 0f)
        {
            rotationFactor = rotationLock;
            slotLockIn = false;
            if (CanDrop())
            {
                Vector2 pos = ((Vector2)this.transform.position + (this.transform.GetChild(0).GetComponent<SegmentCell>().GetSlotPositionOffset()));
                Debug.Log(Vector2.Distance(pos, Input.mousePosition));
                if (!slotLockIn && Vector2.Distance(pos, Input.mousePosition) < 45)
                {
                    slotLockIn = true;
                    rotationFactor = 0;
                    dragPosition = pos;
                }
            }
        }
    }

    /// <summary>
    /// Overides method to shrink based on number of shape segments
    /// </summary>
    public override void ShrinkForInventory()
    {
        float maxSideLength = (shape.GetLength(1) > shape.GetLength(0)) ? shape.GetLength(1) : shape.GetLength(0);
        this.transform.localScale = new Vector3(0.80f / (maxSideLength - 2), 0.80f / (maxSideLength - 2), 0.80f / (maxSideLength - 2));
    }

    #region Dragging Functions

    public override void OnBeginDrag(PointerEventData eventData)
    {
        base.OnBeginDrag(eventData);
        Dragging = true;
        RemoveFromSlots();

        Shrink = false;
    }

    public override void OnDrag(PointerEventData eventData)
    {
        if (!slotLockIn)
        {
            base.OnDrag(eventData);
        }
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        if (CanDrop() && AddToSlots())
        {
            this.anchorPoint = (this.transform as RectTransform).anchoredPosition + (this.transform.GetChild(0).GetComponent<SegmentCell>().GetSlotPositionOffset());
            // TODO Add add to spell
        }
        else
        {
            // Can't drop anywhere so 
            if (this.transform.parent.gameObject == GameObject.Find("Inventory"))
            {
               this.ShrinkForInventory();
                this.anchorPoint = Vector2.zero;
                GameObject.Find("Inventory").GetComponent<InventoryManager>().AddToInventory(this);
            }
        }
        this.GetComponent<Image>().raycastTarget = true;
        Debug.Log("end drag");
        Dragging = false;
    }
    #endregion Dragging Functions

    /// <summary>
    /// Adds this spell item to the under segments and item following lists
    /// </summary>
    /// <returns></returns>
    private bool AddToSlots()
    {
        foreach (GameObject segment in solidBlockObjects)
        {
            Slot slot = segment.GetComponent<SegmentCell>().GetSlotUnder();
            if (slot == null)
            {
                RemoveFromSlots();
                return false;
            }
            this.OccupiedSlots.Add(slot);
            slot.IsOccupied = true;
        }
        foreach (GameObject segment in inputObjectPairs.Keys)
        {
            SegmentCell outputCell = segment.GetComponent<SegmentCell>().GetOutputUnder();
            if (outputCell == null)
            {
                RemoveFromSlots();
                return false;
            }
            outputCell.ParentItem.followingItems.Add(this);
        }
        return true;
    }

    /// <summary>
    /// Checks if the dragged item can be dropped
    /// </summary>
    /// <returns></returns>
    private bool CanDrop()
    {
        if (!((this.rotationModifier % 90) < 5f && (this.rotationModifier % 90) > -5f))
        {
            return false;
        }
        foreach (GameObject segment in solidBlockObjects)
        {
            if (!segment.GetComponent<SegmentCell>().CheckSlotFree())
                return false;
        }
        foreach (GameObject segment in inputObjectPairs.Keys)
        {
            if (!segment.GetComponent<SegmentCell>().CheckSlotFree())
                return false;
        }
        return true;
    }

    /// <summary>
    /// Rotates the shape 90 degrees
    /// </summary>
    public void RotateSegment()
    {
        this.shape = RotateMatrix(this.shape, this.shape.GetLength(0));
    }

    /// <summary>
    /// Rotates the array to match in game object rotation
    /// </summary>
    /// <param name="matrix"></param>
    /// <param name="n"></param>
    /// <returns></returns>
    static Segment[,] RotateMatrix(Segment[,] matrix, int n)
    {
        Segment[,] ret = new Segment[n, n];

        for (int i = 0; i < n; ++i)
        {
            for (int j = 0; j < n; ++j)
            {
                ret[i, j] = matrix[n - j - 1, i];
            }
        }

        return ret;
    }

    /// <summary>
    /// Added remove functionality for input spell items
    /// </summary>
    public override void RemoveFromSlots()
    {
        base.RemoveFromSlots();
        foreach (SpellItem item in followingItems)
        {
            item.RemoveFromSlots();
            item.anchorPoint = Vector2.zero;
            item.ShrinkForInventory();
            GameObject.Find("Inventory").GetComponent<InventoryManager>().AddToInventory(item);
        }
        followingItems.Clear();
    }

    /// <summary>
    ///Generates the piece from spell segment variable
    /// </summary>
    public void CreateSpellSegmentPiece()
    {
        float size = solidImage.gameObject.GetComponent<RectTransform>().rect.width;
        int width = this.shape.GetLength(0) - 1;
        int height = this.shape.GetLength(1) - 1;
        this.GetComponent<RectTransform>().sizeDelta = new Vector2(42 * (width - 1), 43 * (height - 1));
        for (int i = 0; i < width + 1; i++)
        {
            for (int j = 0; j < height + 1; j++)
            {
                Segment segment = this.shape[i, j];
                if (segment != Segment.Empty)
                {
                    if (segment == Segment.Solid)
                    {
                        GameObject itemImage = Instantiate(solidImage);
                        itemImage.transform.SetParent(this.transform, false);
                        itemImage.transform.SetAsFirstSibling();
                        RectTransform imageRect = itemImage.GetComponent<RectTransform>();
                        imageRect.localScale.Set(1, 1, 1);
                        itemImage.transform.localPosition = Vector3.zero +
                            new Vector3(itemImage.GetComponent<RectTransform>().rect.width * (-(width / 2) + i), itemImage.GetComponent<RectTransform>().rect.width * (-(width / 2) + j))
                            - new Vector3(-((width % 2) * 15), (width % 2) * 15);
                        itemImage.transform.localPosition += new Vector3(-itemImage.GetComponent<RectTransform>().rect.width, 0) * ((shape.GetLength(0) + 1) % 2);
                        itemImage.GetComponent<SegmentCell>().Segment = segment;
                        this.solidBlockObjects.Add(itemImage);
                    }
                    else
                    {
                        GameObject itemImage = Instantiate(inputOutputImage);
                        itemImage.transform.SetParent(this.transform, false);
                        itemImage.transform.SetAsFirstSibling();
                        RectTransform imageRect = itemImage.GetComponent<RectTransform>();

                        imageRect.localScale.Set(1, 1, 1);
                        itemImage.transform.localPosition = Vector3.zero +
                            new Vector3(itemImage.GetComponent<RectTransform>().rect.width * (-(width / 2) + i), itemImage.GetComponent<RectTransform>().rect.width * (-(width / 2) + j))
                            - new Vector3(-((width % 2) * 15), (width % 2) * 15);
                        itemImage.transform.localPosition += new Vector3(-itemImage.GetComponent<RectTransform>().rect.width, 0) * ((shape.GetLength(0) + 1) % 2);
                        itemImage.GetComponent<SegmentCell>().ParentItem = this;
                        itemImage.GetComponent<SegmentCell>().Segment = segment;
                        if (segment == Segment.InputRight)
                        {
                            itemImage.GetComponent<Image>().color = Color.cyan;
                            itemImage.GetComponent<CanvasGroup>().alpha = 0.4f;
                            this.inputObjectPairs.Add(itemImage, Segment.InputRight);
                        }
                        else if (segment == Segment.InputDown)
                        {
                            itemImage.GetComponent<Image>().color = Color.cyan;
                            itemImage.GetComponent<CanvasGroup>().alpha = 0.4f;
                            itemImage.transform.Rotate(0, 0, -90);
                            this.inputObjectPairs.Add(itemImage, Segment.InputDown);
                        }
                        else if (segment == Segment.InputLeft)
                        {
                            itemImage.GetComponent<Image>().color = Color.cyan;
                            itemImage.transform.Rotate(0, 0, 180);
                            itemImage.GetComponent<CanvasGroup>().alpha = 0.4f;
                            this.inputObjectPairs.Add(itemImage, Segment.InputLeft);
                        }
                        else if (segment == Segment.InputUp)
                        {
                            itemImage.GetComponent<Image>().color = Color.cyan;
                            itemImage.transform.Rotate(0, 0, 90);
                            itemImage.GetComponent<CanvasGroup>().alpha = 0.4f;
                            this.inputObjectPairs.Add(itemImage, Segment.InputUp);
                        }
                        else if (segment == Segment.OutputRight)
                        {
                            itemImage.GetComponent<Image>().raycastTarget = true;
                            itemImage.GetComponent<CanvasGroup>().blocksRaycasts = true;
                            itemImage.GetComponent<Image>().color = Color.red;
                            this.solidBlockObjects.Add(itemImage);
                        }
                        else if (segment == Segment.OutputDown)
                        {
                            itemImage.GetComponent<Image>().color = Color.red;
                            itemImage.GetComponent<Image>().raycastTarget = true;
                            itemImage.GetComponent<CanvasGroup>().blocksRaycasts = true;
                            itemImage.transform.Rotate(0, 0, -90);
                            this.solidBlockObjects.Add(itemImage);
                        }
                        else if (segment == Segment.OutputLeft)
                        {
                            itemImage.GetComponent<Image>().color = Color.red;
                            itemImage.GetComponent<Image>().raycastTarget = true;
                            itemImage.GetComponent<CanvasGroup>().blocksRaycasts = true;
                            itemImage.transform.Rotate(0, 0, 180);
                            this.solidBlockObjects.Add(itemImage);
                        }
                        else if (segment == Segment.OutputUp)
                        {
                            itemImage.GetComponent<Image>().color = Color.red;
                            itemImage.GetComponent<Image>().raycastTarget = true;
                            itemImage.GetComponent<CanvasGroup>().blocksRaycasts = true;
                            itemImage.transform.Rotate(0, 0, 90);
                            this.solidBlockObjects.Add(itemImage);
                        }
                    }
                }
            }
        }
    }
}
