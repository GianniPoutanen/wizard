﻿using System.Collections;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class InventoryManager : MonoBehaviour
{
    public GameObject segmentItem;

    [Header("Tablet slot variables")]
    public int height = 3;
    public int width = 3;
    public int tabletSlotOffset = 4;
    public GameObject[,] tabletSlots;
    public GameObject[,] tablet;
    [Header("Inventory slot variables")]
    public int numInvSlots = 3;
    public int invSlotOffset = 4;
    public GameObject[] inventorySlots;
    public GameObject[] inventory;
    [Header("Inventory slot variables")]
    public List<SpellSegment> starterSegments = new List<SpellSegment>();
    public int allowedStarters = 1;

    // Inventory specific objects
    private GameManager gameManager;
    private GameObject SlotImage { get; set; }
    private GameObject SpellObject { get; set; }

    private void Awake()
    {
        SlotImage = AssetDatabase.LoadAssetAtPath<UnityEngine.GameObject>("Assets/Prefabs/UIMenus/Inventory/Slot.prefab");
        SpellObject = GameObject.Find("SpellOrb");
    }

    /// <summary>
    /// Performed when object disabled
    /// </summary>
    private void OnDisable()
    {
        Debug.Log("Closed Inventory");
        if (SpellObject != null)
        {
            SpellObject.GetComponent<SpellEmitter>().UpdateSpell(new List<SpellSegment>());
        }
    }

    /// <summary>
    /// Performed when object is enabled
    /// </summary>
    private void OnEnable()
    {
        Render();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        SlotImage = AssetDatabase.LoadAssetAtPath<UnityEngine.GameObject>("Assets/Prefabs/UIMenus/Inventory/Slot.prefab");
    }

    /// <summary>
    /// Redners objects in the inventory canvas
    /// </summary>
    private void Render()
    {
        if (inventorySlots.Length != numInvSlots || tabletSlots.GetLength(0) != width || tabletSlots.GetLength(1) != height)
        {
            tabletSlots = new GameObject[width, height];
            tablet = new GameObject[width, height];
            inventorySlots = new GameObject[numInvSlots];
            inventory = new GameObject[numInvSlots];
        }
        // Reset all children
        foreach (Transform child in this.transform.Find("InventoryPanel"))
        {
            Destroy(child.gameObject);
        }
        foreach (Transform child in this.transform.Find("TabletPanel"))
        {
            Destroy(child.gameObject);
        }

        // loads inventory slots
        for (int i = 0; i < numInvSlots; i++)
        {
            GameObject invSlot = Instantiate(SlotImage, Vector3.zero, Quaternion.identity);
            invSlot.transform.SetParent(this.transform.Find("InventoryPanel"));
            RectTransform rectPos = invSlot.GetComponent<RectTransform>();
            rectPos.anchoredPosition3D = new Vector3(-((float)width / 2.0f * (rectPos.sizeDelta.x + tabletSlotOffset)) + ((rectPos.sizeDelta.x + tabletSlotOffset) * i) + (rectPos.sizeDelta.x / 2), 0, -1);
            invSlot.GetComponent<Slot>().SlotPosition = new int[] { i };
            invSlot.GetComponent<Slot>().IsInventory = true;
            invSlot.tag = "Inventory";
            invSlot.name = "Inventory: " + i;
            inventorySlots[i] = invSlot;
        }

        // loads tab slots
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                GameObject tabSlot = Instantiate(SlotImage, Vector3.zero, Quaternion.identity);
                tabSlot.transform.SetParent(this.transform.Find("TabletPanel"));
                tabSlot.tag = "Tablet";
                tabSlot.name = "Tablet: " + i + " and " + j;
                RectTransform rectPos = tabSlot.GetComponent<RectTransform>();

                rectPos.anchoredPosition3D = new Vector3(-((float)width / 2.0f * (rectPos.sizeDelta.x + tabletSlotOffset)) +
                                                          ((rectPos.sizeDelta.x + tabletSlotOffset) * i) + (rectPos.sizeDelta.x / 2), //+ (rectPos.sizeDelta.x / 2) + (invSlotOffset / 2.0f),
                                                          -((float)height / 2.0f * (rectPos.sizeDelta.y + tabletSlotOffset)) +
                                                          ((rectPos.sizeDelta.y + tabletSlotOffset) * j) + (rectPos.sizeDelta.y / 2) + (invSlotOffset / 2.0f)
                                                          , -1);

                tabSlot.GetComponent<Slot>().SlotPosition = new int[] { i, j };
                tabletSlots[i, j] = tabSlot;

            }
        }
        AddTestItems();
    }

    /// <summary>
    /// Checks if the Slots in the list of slots are free
    /// </summary>
    /// <param name="slots"></param>
    /// <returns></returns>
    public bool CheckSlotsFree(List<Slot> slots)
    {
        foreach (Slot slot in slots)
        {
            if (slot.IsOccupied)
            {
                return false;
            }
        }
        return true;
    }

    public Slot GetNextInventory()
    {
        foreach (GameObject slot in inventorySlots)
        {
            if (!slot.GetComponent<Slot>().IsOccupied)
            {
                return slot.GetComponent<Slot>();
            }
        }
        return null;
    }

    /// <summary>
    /// Adds item to the next available slot
    /// </summary>
    /// <param name="item"></param>
    public void AddToInventory(Item item)
    {
        Slot availableSlot = GetNextInventory();
        item.transform.SetParent(availableSlot.transform);
        item.Shrink = true;
        item.OccupiedSlots.Add(availableSlot);
        availableSlot.IsOccupied = true;
    }
    
    /// <summary>
    /// Adds item to the next available slot
    /// </summary>
    /// <param name="item"></param>
    public void AddToInventory(SpellItem item)
    {
        Slot availableSlot = GetNextInventory();
        item.transform.SetParent(availableSlot.transform);
        item.Shrink = true;
        item.OccupiedSlots.Add(availableSlot);
        item.dragPosition = availableSlot.transform.position;
        availableSlot.IsOccupied = true;
    }

    ////// TEST FUNCTION
    public void AddTestItems()
    {
        GameObject starterSlot = Instantiate(segmentItem);
        Slot invSlotTemp = this.GetNextInventory();
        starterSlot.transform.SetParent(invSlotTemp.transform);
        starterSlot.GetComponent<RectTransform>().anchoredPosition = Vector2.zero; ;
        starterSlot.GetComponent<SpellItem>().Shrink = true;
        starterSlot.GetComponent<SpellItem>().OccupiedSlots.Add(invSlotTemp);
        starterSlot.GetComponent<SpellItem>().SpellSegment = new SpellSegment();
        invSlotTemp.IsOccupied = true;

        starterSlot = Instantiate(segmentItem);
        invSlotTemp = this.GetNextInventory();
        starterSlot.transform.SetParent(invSlotTemp.transform);
        starterSlot.GetComponent<RectTransform>().anchoredPosition = Vector2.zero; ;
        starterSlot.GetComponent<SpellItem>().shape = ShapeGenerator.GenerateShape(0,1,1);
        starterSlot.GetComponent<SpellItem>().OccupiedSlots.Add(invSlotTemp);
        starterSlot.GetComponent<SpellItem>().SpellSegment = new SpellSegment();
        starterSlot.GetComponent<SpellItem>().Shrink = true;
        invSlotTemp.IsOccupied = true;
    }
}
