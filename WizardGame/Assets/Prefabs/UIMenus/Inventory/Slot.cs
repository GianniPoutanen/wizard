﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Slot : MonoBehaviour, IDropHandler
{
    private bool flash;

    private void Start()
    {
        inventoryManager = GameObject.Find("Inventory").GetComponent<InventoryManager>();
    }

    private void Update()
    {
        occupied = IsOccupied;
    }

    // Item dropped
    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log("Dropped");
        if (eventData.pointerDrag != null)
        {
            // change from anchor point to one of the shape positions
            if (!IsOccupied)
            {
                // call the shrunk thing
                if (IsInventory)
                {
                    eventData.pointerDrag.GetComponent<Item>().Shrink = true;
                    eventData.pointerDrag.GetComponent<Transform>().parent = this.transform;
                    eventData.pointerDrag.GetComponent<Item>().OccupiedSlots.Add(this);
                    this.IsOccupied = true;
                }
            }
        }
    }
    public bool occupied;
    public bool IsOccupied { get; set; } = false;
    public bool IsInventory { get; set; }
    public int[] SlotPosition { get; set; }
    public InventoryManager inventoryManager;

    public bool Flash
    {
        get
        {
            return flash;
        }
        set
        {
            if (flash)
            {
                this.gameObject.GetComponent<Image>().color = new Color(255, 255, 255);
            }
            else
            {
                this.gameObject.GetComponent<Image>().color = new Color(0, 0, 100);
            }
            flash = value;
        }
    }

}
