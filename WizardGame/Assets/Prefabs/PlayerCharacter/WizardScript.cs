﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WizardScript : MonoBehaviour
{
    public float speed;
    public Vector2 movementDirection;
    private Rigidbody2D rb2D;

    // Game pause variables
    private GameManager gameManager;
    private Vector2 savedVelocity;
    private bool velocitySaved;

    // Dash variables
    private bool canDash;
    private bool dashing;
    public float dashSpeed;
    private float dashTimer;
    public float dashTime;
    private float dashingTimer;
    private float dashDelay;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        rb2D = this.GetComponent<Rigidbody2D>();
        canDash = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (dashing)
        {
            if (dashingTimer > Time.time)
            {
                Debug.Log("Dashing");
                //this.rb2D.AddForce(movementDirection * speed);
            }
            else
            {
                dashing = false;
            }
        }
        if (!gameManager.Paused)
        {
            if (velocitySaved)
            {
                this.rb2D.velocity += savedVelocity;
                velocitySaved = false;
            }
            if (CheckKeyMovement())
            {
                CheckDash();
            }
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.y);
        }
        else
        {
            if (!velocitySaved)
            {
                savedVelocity = this.rb2D.velocity;
                velocitySaved = true;
            }
            this.rb2D.velocity = Vector2.zero;
        }
    }

    /// <summary>
    /// Player dash script based on space
    /// </summary>
    /// <returns></returns>
    private void CheckDash()
    {
        if (dashTimer < Time.time)
        {
            canDash = true;
            dashTimer = Time.time + dashDelay;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            this.rb2D.velocity = (movementDirection * dashSpeed);
            dashingTimer = Time.time + dashTime;
            dashing = true;
        }
    }

    /// <summary>
    /// Player movement script based on key inputs
    /// </summary>
    private bool CheckKeyMovement()
    {
        if (Input.GetKey(KeyCode.W))
        {
            if (Input.GetKey(KeyCode.D))
            {
                movementDirection = new Vector2(0.7092f, 0.7092f);
                rb2D.AddForce(movementDirection * speed);
                return true;
            }
            else if (Input.GetKey(KeyCode.A))
            {
                movementDirection = new Vector2(-0.7092f, 0.7092f);
                rb2D.AddForce(movementDirection * speed);
                return true;
            }
            else
            {
                movementDirection = new Vector2(0, 1);
                rb2D.AddForce(movementDirection * speed);
                return true;
            }
        }
        else if (Input.GetKey(KeyCode.S))
        {
            if (Input.GetKey(KeyCode.D))
            {
                movementDirection = new Vector2(0.7092f, -0.7092f);
                rb2D.AddForce(movementDirection * speed);
                return true;
            }
            if (Input.GetKey(KeyCode.A))
            {
                movementDirection = new Vector2(-0.7092f, -0.7092f);
                rb2D.AddForce(movementDirection * speed);
                return true;
            }
            else
            {
                movementDirection = new Vector2(0, -1);
                rb2D.AddForce(movementDirection * speed);
                return true;
            }
        }
        else if (Input.GetKey(KeyCode.D))
        {
            movementDirection = new Vector2(1, 0);
            rb2D.AddForce(movementDirection * speed);
            return true;
        }
        else if (Input.GetKey(KeyCode.A))
        {
            movementDirection = new Vector2(-1, 0);
            rb2D.AddForce(movementDirection * speed);
            return true;
        }
        else
        {
            movementDirection = Vector2.zero;
            return false;
        }
    }
}
