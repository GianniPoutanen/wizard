﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class SpellEmitter : MonoBehaviour
{
    List<SpellSegment> spells = new List<SpellSegment>();
    List<float> castTimers = new List<float>();
    List<List<int>> chargedSpells = new List<List<int>>();
    List<List<float>> delayTimers = new List<List<float>>();
    List<float> reloadTimers = new List<float>();
    List<bool> reloadChecks = new List<bool>();
    List<int> ammoChecker = new List<int>();

    public GameManager gameManager;


    // Start is called before the first frame update
    void Start()
    {
        UpdateSpell(new List<SpellSegment>());
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!gameManager.Paused)
        {
            // Reload button
            if (Input.GetKeyDown(KeyCode.R))
            {
                for (int i = 0; i < spells.Count; i++)
                {
                    if (ammoChecker[i] != spells[i].spellStats.ammo)
                    {
                        Debug.Log("Reloading");
                        reloadTimers[i] = spells[i].spellStats.reloadTime + Time.time;
                        reloadChecks[i] = true;
                    }
                }
            }

            // Start firing
            if (Input.GetMouseButtonDown(0))
            {
                for (int i = 0; i < spells.Count; i++)
                {
                    castTimers[i] = (Time.time + spells[i].spellStats.chargeTime);
                    chargedSpells[i].Clear();
                    delayTimers[i].Clear();
                    
                    if (spells[i].singleFire && !reloadChecks[i])
                    {
                        chargedSpells[i].Add(spells[i].numProjectiles);
                        delayTimers[i].Add(Time.time);
                        ammoChecker[i]--;
                    }
                }
            }

            // Hold fire
            if (Input.GetMouseButton(0))
            {
                for (int i = 0; i < spells.Count; i++)
                {
                    if (castTimers[i] < Time.time && !reloadChecks[i])
                    {
                        chargedSpells[i].Add(spells[i].numProjectiles);
                        delayTimers[i].Add(Time.time);
                        castTimers[i] = Time.time + spells[i].spellStats.chargeTime;
                        ammoChecker[i]--;
                    }
                }
            }

            for (int i = 0; i < spells.Count; i++)
            {
                for (int j = 0; j < chargedSpells[i].Count; j++)
                {
                    if (chargedSpells[i][j] > 0 && delayTimers[i][j] < Time.time)
                    {
                        spells[i].spellStartPosition = this.transform.position;
                        spells[i].CastSpell();
                        if (ammoChecker[i] == 0)
                        {
                            Debug.Log("Reloading");
                            reloadTimers[i] = spells[i].spellStats.reloadTime + Time.time;
                            reloadChecks[i] = true;
                        }

                        spells[i].ammo = spells[i].spellStats.ammo;
                        chargedSpells[i][j]--;
                        delayTimers[i][j] = Time.time + spells[i].projectileDelay;
                        if (chargedSpells[i][j] == 0)
                        {
                            chargedSpells[i].Remove(j);
                            delayTimers[i].Remove(j);
                            j--;
                        }
                    }
                }
            }

            // Reload
            for (int i = 0; i < spells.Count; i++)
            {
                if (reloadChecks[i] && reloadTimers[i] < Time.time)
                {
                    Debug.Log("Reloaded");
                    reloadChecks[i] = false;
                    ammoChecker[i] = spells[i].spellStats.ammo;
                    castTimers[i] = Time.time + spells[i].spellStats.chargeTime;
                }
            }
        }
    }

    /// <summary>
    /// Updates the spell to the current spell segment
    /// </summary>
    /// <param name="spellStarters"></param>
    public void UpdateSpell(List<SpellSegment> spellStarters)
    {
        this.spells.Clear();
        if (spellStarters.Count == 0)
        {
            SpellSegment SpellSegment = new SpellSegment();
            spellStarters.Add(new SpellSegment());
        }
        foreach (SpellSegment segment in spellStarters)
        {
            segment.UpdateSpellStats();
            this.spells.Add(segment);
        }
        castTimers.Clear();
        chargedSpells.Clear();
        delayTimers.Clear();
        ammoChecker.Clear();
        reloadTimers.Clear();
        reloadChecks.Clear();
        for (int i = 0; i < spells.Count; i++)
        {
            castTimers.Add(Time.time + spells[i].spellStats.chargeTime);
            chargedSpells.Add(new List<int>());
            delayTimers.Add(new List<float>());
            chargedSpells[i].Add(0);
            delayTimers[i].Add(Time.time);
            ammoChecker.Add(spells[i].spellStats.ammo);
            reloadTimers.Add(spells[i].spellStats.reloadTime + Time.time);
            reloadChecks.Add(true);
        }
    }
}


