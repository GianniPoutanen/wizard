﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    public float maxRadius;
    private float radius;
    public float minSpeed;
    private float speed;
    public GameObject anchorObj;

    private GameManager gameManager;

    // Start is called before the first frame update
    private void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        anchorObj = GameObject.Find("SpellOrb");
    }

    // Update is called once per frame
    void Update()
    {
        if (!gameManager.Paused)
        {
            MovementScript();
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.y - 7);
        }
    }

    /// <summary>
    /// How the Camera moves 
    /// </summary>
    private void MovementScript()
    {
        Vector2 camPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 anchPos = anchorObj.transform.position;
        float distance = 0;
        distance = Vector2.Distance(anchPos, camPos) / 4;
        if (distance > maxRadius)
        {
            radius = maxRadius;
        }
        else
        {
            radius = distance;
        }
        float Degrees = Mathf.Deg2Rad * AngleBetweenVector2(anchPos, camPos);
        float x = anchorObj.transform.position.x + radius * Mathf.Cos(Degrees);
        float y = anchorObj.transform.position.y + radius * Mathf.Sin(Degrees);
        speed = minSpeed * Vector2.Distance(this.transform.position, new Vector2(x, y));
        this.transform.position = Vector2.MoveTowards(this.transform.position, new Vector2(x, y), speed);
    }

    /// <summary>
    /// Angle Between two points
    /// </summary>
    /// <param name="vec1">First point</param>
    /// <param name="vec2">Second point</param>
    /// <returns></returns>
    private float AngleBetweenVector2(Vector2 vec1, Vector2 vec2)
    {
        Vector2 diference = vec2 - vec1;
        float sign = (vec2.y < vec1.y) ? -1.0f : 1.0f;
        return Vector2.Angle(Vector2.right, diference) * sign;
    }
}
