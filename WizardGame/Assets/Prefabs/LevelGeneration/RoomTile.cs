﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomTile : MonoBehaviour
{
    public int[] tilePosition = new int[2];
    public int ID = 0;

    /// Collider child objects
    public GameObject topCollider;
    public GameObject botCollider;
    public GameObject leftCollider;
    public GameObject rightCollider;


    public enum eRoomType
    {
        Default,
        HallEntrance,
        Hall
    }


    public enum eTileType
    {
        TopWall,
        OpenTile,
        LeftWall,
        RightWall,
        BotWall
    }

    public eRoomType roomType = eRoomType.Default;
    public eTileType tileType = eTileType.OpenTile;
    
    /// <summary>
    /// Plays on object start
    /// </summary>
    private void Start()
    {
        this.topCollider = this.transform.Find("TopCollider").gameObject;
        this.botCollider = this.transform.Find("BotCollider").gameObject;
        this.leftCollider = this.transform.Find("LeftCollider").gameObject;
        this.rightCollider = this.transform.Find("RightCollider").gameObject;
    }

    public void SetTileType(eTileType type)
    {
        this.tileType = type;
        switch (type)
        {
            case eTileType.OpenTile:

                break;
        }
    }
}

