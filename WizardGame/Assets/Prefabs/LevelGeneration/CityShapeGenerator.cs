﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class CityShapeGenerator : MonoBehaviour
{
    // Perlin noise variables
    public int width;
    public int height;
    public float scale;
    public float offsetX;
    public float offsetY;
    public List<List<int[]>> islands;

    [Range(0.0f, 1.0f)]
    public float cullingVal;

    // Mapped arrays
    public float[,] noiseMap;
    public GameObject[,] objectMap;

    public GameObject roomTile;

    public int corridorRemovalSize;
    public int maxCorridorLength;
    public bool squareRooms;
    public bool ridCracks;
    public bool removeCorridors;

    public int islandSizeLimiter;

    // Start is called before the first frame update
    void Start()
    {
        CreateMap();
        while (islands.Count != 1)
        {
            for (int i = Random.Range(1, 3); i > 0; i--)
            {
                GenerateRandomSimpleCorridor();
            }
            islands = CountIslands(objectMap);
        }
        Debug.Log("Map Size = " + CountArea());
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            CreateMap();
            while (islands.Count != 1)
            {
                for (int i = Random.Range(1, 3); i > 0; i--)
                {
                    GenerateRandomSimpleCorridor();
                }
                islands = CountIslands(objectMap);
            }
        }

        if (Input.GetKeyDown(KeyCode.T))
        {
            foreach (List<int[]> island in islands)
            {
                int[] pos = AverageIslandPos(island);
                pos = FindNearestRoomFromPos(objectMap, pos[0], pos[1]);
                DrawMarkerAtPoint(pos[0], pos[1]);
            }

        }

        if (Input.GetKeyDown(KeyCode.Y))
        {
            while (islands.Count != 1)
            {
                for (int i = Random.Range(1, 3); i > 0; i--)
                {
                    GenerateRandomSimpleCorridor();
                }
                islands = CountIslands(objectMap);
            }
        }

        //TODO: test isWedge and is corner
        if (Input.GetMouseButtonDown(0))
        {
            int[] coords = GetWorldToIndex(Camera.main.ScreenToWorldPoint(Input.mousePosition));
            List<int[]> island = GetIslandFromPos(coords[0], coords[1]);
            Debug.Log("This islands wedges count to - " + CountWedges(island));
        }
    }

    /// <summary>
    /// Calls and creates a given map and destroys previous map
    /// </summary>
    private void CreateMap()
    {
        Debug.Log("Generating Map");
        islands = new List<List<int[]>>();
        while (!(islands.Count >= 5 && islands.Count < 7 && CountArea() > 1200))
        {
            foreach (Transform child in this.transform)
            {
                Destroy(child.gameObject);
            }
            Randomise();
            GenerateLayout();
        }
    }
    /// <summary>
    /// Randomises the layout by setting offsets
    /// </summary>
    public void Randomise()
    {
        offsetX = Random.Range(0f, 9999f);
        offsetY = Random.Range(0f, 9999f);
    }

    /// <summary>
    /// Generates the layout of the city
    /// </summary>
    public void GenerateLayout()
    {
        int numTiles = 1;
        objectMap = new GameObject[width, height];
        PerlinNoiseMap();
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                float mapInt = noiseMap[x, y];
                numTiles++;
                if (cullingVal < mapInt)
                {
                    numTiles--;
                    GameObject obj = CreateTileAtPos(RoomTile.eRoomType.Default, x, y);
                    obj.GetComponent<SpriteRenderer>().color = new Color(mapInt, mapInt, mapInt);
                }
            }
        }
        //RemoveBorderRedundants(objectMap);
        if (squareRooms)
            for (int i = 0; i < 5; i++)
                SquareIslands(objectMap);

        if (removeCorridors)
            for (int i = 0; i < 5; i++)
                RemoveCorridors(objectMap, corridorRemovalSize);
        if (ridCracks)
            for (int i = 0; i < 5; i++)
                GetRidOfCracks(objectMap);
        islands = CountIslands(objectMap);
        DeleteIslandsOfSize(islandSizeLimiter);
        Debug.Log("Num Islands = " + islands.Count);
    }

    /// <summary>
    /// Creates an array map of noise
    /// </summary>
    /// <returns></returns>
    public float[,] PerlinNoiseMap()
    {
        noiseMap = new float[width, height];

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                float xCoord = (float)x / width * scale + offsetX;
                float yCoord = (float)y / height * scale + offsetY;
                noiseMap[x, y] = Mathf.PerlinNoise(xCoord, yCoord);
            }
        }
        return noiseMap;
    }


    #region Island Counting code - https://www.geeksforgeeks.org/find-number-of-islands/ - (Changed to return a list of islands List<int[2]>

    // No of rows 
    // and columns 

    // A function to check if 
    // a given cell (row, col) 
    // can be included in DFS 
    bool isSafe(GameObject[,] M, int row,
                       int col, bool[,] visited)
    {
        // row number is in range, 
        // column number is in range 
        // and value is 1 and not 
        // yet visited 

        return (row >= 0) && (row < noiseMap.GetLength(0)) && (col >= 0) && (col < noiseMap.GetLength(1)) && (M[row, col] != null && !visited[row, col]);
    }

    // A utility function to do 
    // DFS for a 2D boolean matrix. 
    // It only considers the 8 
    // neighbors as adjacent vertices 
    List<int[]> DFS(GameObject[,] M, int row,
                    int col, bool[,] visited)
    {
        // These arrays are used to 
        // get row and column numbers 
        // of 8 neighbors of a given cell 
        int[] rowNbr = new int[] { -1, -1, -1, 0,
                                   0, 1, 1, 1 };
        int[] colNbr = new int[] { -1, 0, 1, -1,
                                   1, -1, 0, 1 };

        // Mark this cell 
        // as visited 
        visited[row, col] = true;
        List<int[]> island = new List<int[]>();
        island.Add(new int[] { row, col });

        // Recur for all 
        // connected neighbours 
        for (int k = 0; k < 8; ++k)
            if (isSafe(M, row + rowNbr[k], col + colNbr[k], visited))
            {
                island.AddRange(DFS(M, row + rowNbr[k], col + colNbr[k], visited));
            }
        return island;
    }

    // The main function that 
    // returns count of islands 
    // in a given boolean 2D matrix 
    private List<List<int[]>> CountIslands(GameObject[,] M)
    {
        // Make a bool array to 
        // mark visited cells. 
        // Initially all cells 
        // are unvisited 
        bool[,] visited = new bool[noiseMap.GetLength(0), noiseMap.GetLength(1)];
        int islandID = 1;

        // Initialize count as 0 and 
        // travese through the all 
        // cells of given matrix 
        List<List<int[]>> islandsList = new List<List<int[]>>();
        for (int i = 0; i < noiseMap.GetLength(0); ++i)
            for (int j = 0; j < noiseMap.GetLength(1); ++j)
                if (M[i, j] != null && !visited[i, j])
                {
                    // If a cell with value 1 is not 
                    // visited yet, then new island 
                    // found, Visit all cells in this 
                    // island and increment island count 
                    List<int[]> island = DFS(M, i, j, visited);
                    islandsList.Add(island);
                    foreach (int[] coord in island)
                    {
                        M[coord[0], coord[1]].GetComponent<RoomTile>().ID = islandID;
                    }
                    islandID++;
                }

        return islandsList;
    }
    #endregion

    #region Sqaure Off Code

    // No of rows 
    // and columns 

    // A function to check if 
    // a given cell (row, col) 
    // can be included in DFS 
    bool SquareOff(GameObject[,] M, int row,
                       int col, bool[,] visited)
    {
        // These arrays are used to 
        // get row and column numbers 
        // of 8 neighbors of a given cell 
        int[] rowNbr = new int[] { -1, -1, -1, 0,
                                   0, 1, 1, 1 };
        int[] colNbr = new int[] { -1, 0, 1, -1,
                                   1, -1, 0, 1 };

        // Recur for all 
        // connected neighbours 
        for (int k = 0; k < 8; ++k)
            if (InRange(row + rowNbr[k], col + colNbr[k]) && M[row + rowNbr[k], col + colNbr[k]] != null)


                // row number is in range, 
                // column number is in range 
                // and value is 1 and not 
                // yet visited 
                if (InRange(row, col) && M[row, col] == null && IsCorner(M, row, col) && !IsWedge(M, row, col))
                {
                    GameObject obj = CreateTileAtPos(RoomTile.eRoomType.Default, row, col);
                    obj.GetComponent<SpriteRenderer>().color = new Color(255, 0, 0);
                }

        return (row >= 0) && (row < noiseMap.GetLength(0)) && (col >= 0) && (col < noiseMap.GetLength(1)) && !visited[row, col];
    }

    // A utility function to do 
    // DFS for a 2D boolean matrix. 
    // It only considers the 8 
    // neighbors as adjacent vertices 
    void DFSSquareOff(GameObject[,] M, int row,
                    int col, bool[,] visited)
    {
        // These arrays are used to 
        // get row and column numbers 
        // of 8 neighbors of a given cell 
        int[] rowNbr = new int[] { -1, -1, -1, 0,
                                   0, 1, 1, 1 };
        int[] colNbr = new int[] { -1, 0, 1, -1,
                                   1, -1, 0, 1 };

        // Mark this cell 
        // as visited 
        visited[row, col] = true;

        // Recur for all 
        // connected neighbours 
        for (int k = 0; k < 8; ++k)
            if (SquareOff(M, row + rowNbr[k], col + colNbr[k], visited))
                DFSSquareOff(M, row + rowNbr[k],
                    col + colNbr[k], visited);
    }

    /// <summary>
    /// Square islands off
    /// </summary>
    /// <param name="M"></param>
    /// <returns></returns>
    void SquareIslands(GameObject[,] M)
    {
        // Make a bool array to 
        // mark visited cells. 
        // Initially all cells 
        // are unvisited 
        bool[,] visited = new bool[M.GetLength(0), M.GetLength(1)];

        // Initialize count as 0 and 
        // travese through the all 
        // cells of given matrix 
        for (int i = 0; i < M.GetLength(0); ++i)
            for (int j = 0; j < M.GetLength(1); ++j)
                if (M[i, j] != null && !visited[i, j])
                {
                    // If a cell with value 1 is not 
                    // visited yet, then new island 
                    // found, Visit all cells in this 
                    // island and increment island count 
                    DFSSquareOff(M, i, j, visited);
                }

    }

    /// <summary>
    /// returns true if the row and col are in the range of the width and length of the array
    /// </summary>
    /// <param name="row"></param>
    /// <param name="col"></param>
    /// <returns></returns>
    private bool InRange(int row, int col)
    {
        return (row >= 0) && (row < noiseMap.GetLength(0)) && (col >= 0) && (col < noiseMap.GetLength(1));
    }

    /// <summary>
    /// returns true if the cell is a wedge
    /// </summary>
    /// <returns></returns>
    private bool IsWedge(GameObject[,] M, int row, int col)
    {
        int[,] rowNbr = { { -1, -1, -1, 0, 1 }, { -1, 0, 1, 1, 1 }, { 1, 1, 1, 0, -1 }, { 1, 0, -1, -1, -1 } };
        int[,] colNbr = { { -1, 0, 1, 1, 1 }, { 1, 1, 1, 0, -1 }, { 1, 0, -1, -1, -1 }, { -1, -1, -1, 0, 1 } };

        for (int k = 0; k < 4; k++)
        {
            for (int j = 0; j < 5; j++)
            {
                if (InRange(row + rowNbr[k, j], col + colNbr[k, j]) && M[row + rowNbr[k, j], col + colNbr[k, j]] == null)
                {
                    break;
                }
                if (j == 4)
                {
                    return true;
                }
            }
        }
        return false;
    }

    /// <summary>
    /// returns true if the cell is a wedge
    /// </summary>
    /// <returns></returns>
    private bool IsCorner(GameObject[,] M, int row, int col)
    {
        int[,] rowNbr = { { -1, -1, 0 }, { 0, 1, 1 }, { 1, 1, 0 }, { 0, -1, -1 } };
        int[,] colNbr = { { 0, 1, 1 }, { 1, 1, 0 }, { 0, -1, -1 }, { -1, -1, 0 } };

        for (int k = 0; k < 4; k++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (InRange(row + rowNbr[k, j], col + colNbr[k, j]) && M[row + rowNbr[k, j], col + colNbr[k, j]] == null)
                {
                    break;
                }
                if (j == 2)
                {
                    return true;
                }
            }
        }
        return false;
    }

    /// <summary>
    /// Gets rid of cracks between rooms
    /// </summary>
    /// <param name="M"></param>
    private void GetRidOfCracks(GameObject[,] M)
    {
        for (int i = 0; i < M.GetLength(0); ++i)
            for (int j = 0; j < M.GetLength(1); ++j)
                if (M[i, j] == null && CountNeighbours(M, i, j, true) >= 6)
                {
                    GameObject obj = CreateTileAtPos(RoomTile.eRoomType.Default, i, j);
                    obj.GetComponent<SpriteRenderer>().color = new Color(255, 0, 0);
                }
    }

    /// <summary>
    /// Counts the number of wedges int he island
    /// </summary>
    /// <param name="island"></param>
    /// <returns></returns>
    private int CountWedges(List<int[]> island)
    {
        int numWedges = 0;
        foreach (int[] coords in island)
        {
            if (CountNeighbours(objectMap, coords[0], coords[1], false) == 7)
            {
                numWedges++;
            }
        }
        return numWedges;
    }

    #endregion Square Off Code

    #region Remove Rendundant Code


    /// <summary>
    /// Square islands off
    /// </summary>
    /// <param name="M"></param>
    /// <returns></returns>
    void RemoveBorderRedundants(GameObject[,] M)
    {
        // Make a bool array to 
        // mark visited cells. 
        // Initially all cells 
        // are unvisited 
        bool[,] visited = new bool[M.GetLength(0), M.GetLength(1)];

        // Initialize count as 0 and 
        // travese through the all 
        // cells of given matrix 
        for (int i = 0; i < M.GetLength(0); ++i)
            for (int j = 0; j < M.GetLength(1); ++j)
                if (CountNeighbours(M, i, j, false) < 3)
                {
                    GameObject.Destroy(M[i, j]);
                    M[i, j] = null;
                }
    }

    /// <summary>
    /// Removes the corridors given by corridor size
    /// </summary>
    /// <param name="M"></param>
    /// <param name="corridorSize"></param>
    private void RemoveCorridors(GameObject[,] M, int corridorSize)
    {
        for (int i = 0; i < M.GetLength(0); ++i)
            for (int j = 0; j < M.GetLength(1); ++j)
                if (M[i, j] != null && (IsHorizontalCorridors(M, i, j, corridorSize) || IsCerticleCorridors(M, i, j, corridorSize)))
                {
                    Destroy(M[i, j]);
                    M[i, j] = null;
                }
    }
    ///
    /// <summary>
    /// Returns true when detecting a corridor that is of given size
    /// </summary>
    /// <param name="M"></param>
    /// <param name="row"></param>
    /// <param name="col"></param>
    /// <param name="corridorSize"></param>
    /// <returns></returns>
    private bool IsHorizontalCorridors(GameObject[,] M, int row, int col, int corridorSize)
    {
        if ((!InRange(row, col - 1) || M[row, col - 1] == null))
        {
            for (int i = 0; i < corridorSize + 1; i++)
                if (!InRange(row, col + i) || M[row, col + i] == null)
                    return true;
        }
        return false;
    }
    /// <summary>
    /// Returns true when detecting a corridor that is of given size
    /// </summary>
    /// <param name="M"></param>
    /// <param name="row"></param>
    /// <param name="col"></param>
    /// <param name="corridorSize"></param>
    /// <returns></returns>
    private bool IsCerticleCorridors(GameObject[,] M, int row, int col, int corridorSize)
    {
        if ((!InRange(row - 1, col) || M[row - 1, col] == null))
        {
            for (int i = 0; i < corridorSize + 1; i++)
                if (!InRange(row + i, col) || M[row + i, col] == null)
                    return true;
        }
        return false;
    }

    /// <summary>
    /// Deletes islands smaller than or equal to given size
    /// </summary>
    /// <param name="size"></param>
    private void DeleteIslandsOfSize(int size)
    {
        for (int i = 0; i < islands.Count; i++)
        {
            List<int[]> island = islands[i];
            if (island.Count <= size)
            {
                foreach (int[] coord in island)
                {
                    Destroy(objectMap[coord[0], coord[1]]);
                    objectMap[coord[0], coord[1]] = null;
                }
                islands.RemoveAt(i);
                i--;
            }
        }
    }

    #endregion Remove Rendundant Code

    #region Corridor Creation Code

    /// <summary>
    /// Returns true if there's a corridor from given position to island, -1/left, 1/right
    /// </summary>
    /// <param name="island"></param>
    /// <param name="row"></param>
    /// <param name="col"></param>
    /// <returns></returns>
    private bool CheckCorridorToIsland(List<int[]> endIsland, int row, int col, int[] direction, bool colour)
    {

        bool islandFound = false;
        int length = 0;
        int currentRow = row + (direction[0] * length);
        int currentCol = col + (direction[1] * length);
        while (!islandFound && InRange(currentRow, currentCol))
        {
            currentRow = row + (direction[0] * length);
            currentCol = col + (direction[1] * length);

            List<int[]> neighbours = GetNeighbours(objectMap, currentRow, currentCol);
            foreach (int[] coord in neighbours)
            {
                if (GetIslandFromPos(coord[0], coord[1]).Count > 0)
                {
                    if (InIsland(endIsland, coord[0], coord[1]))
                    {
                        if (neighbours.Count != 3 && neighbours.Count != 0)
                            return false;
                        if (colour)
                            objectMap[coord[0], coord[1]].GetComponent<SpriteRenderer>().color = new Color(100, 100, 250);
                            objectMap[coord[0], coord[1]].GetComponent<RoomTile>().roomType = RoomTile.eRoomType.HallEntrance;

                        islandFound = true; // found island
                    }
                    else
                    {
                        return false; // island found but the right one
                    }
                }
            }

            length++;
        }
        return islandFound;
    }


    /// <summary>
    /// Returns true if there's a corridor from given position to island, -1,0 left - 1,0 right - 0,-1 up - 0,1 down
    /// </summary>
    /// <param name="endIsland"></param>
    /// <param name="row"></param>
    /// <param name="col"></param>
    /// <returns></returns>
    private void CreateCorridorToIsland(List<int[]> endIsland, int row, int col, int[] direction)
    {
        bool islandFound = false;
        int length = 0;
        while (!islandFound)
        {
            int currentRow = row + (direction[0] * length);
            int currentCol = col + (direction[1] * length);
            if (InRange(currentRow, currentCol))
            {
                List<int[]> neighbours = GetNeighbours(objectMap, currentRow, currentCol);
                foreach (int[] coord in neighbours)
                {
                    if (InIsland(endIsland, coord[0], coord[1]))
                    {
                        islandFound = true;
                    }
                }

                if (!islandFound)
                {
                    CreateCorridorBlock(currentRow, currentCol);
                }
            }
            length++;
        }
    }


    /// <summary>
    /// Creates a block for the corridor
    /// </summary>
    /// <param name="row"></param>
    /// <param name="col"></param>
    private void CreateCorridorBlock(int row, int col)
    {
        int[] rowNbr = new int[] { 0, -1, -1, -1, 0,
                                   0, 1, 1, 1 };
        int[] colNbr = new int[] { 0, -1, 0, 1, -1,
                                   1, -1, 0, 1 };

        for (int i = 0; i < 9; i++)
        {
            if (objectMap[row + rowNbr[i], col + colNbr[i]] == null)
            {
                GameObject obj = CreateTileAtPos(RoomTile.eRoomType.Hall, row + rowNbr[i], col + colNbr[i]);
            }
        }
    }

    /// <summary>
    /// Randomly finds a suitable position and generates a corridor
    /// </summary>
    /// <returns></returns>
    private List<List<int[]>> GenerateRandomSimpleCorridor()
    {
        bool corridorCheck = false; // used to validate corridor possibilities
        List<List<int[]>> connectableIslands = new List<List<int[]>>();
        int[] randomPos = new int[] { Random.Range(0, objectMap.GetLength(0) - 1), Random.Range(0, objectMap.GetLength(1) - 1) };
        connectableIslands = GetIslandsFromPos(objectMap, randomPos[0], randomPos[1]);
        int attempts = 0;


        while ((GetNeighbours(objectMap, randomPos[0], randomPos[1]).Count != 0 || !corridorCheck) && attempts < 100)
        {
            randomPos = new int[] { Random.Range(0, objectMap.GetLength(0) - 1), Random.Range(0, objectMap.GetLength(1) - 1) };

            List<List<int[]>> tempIsles = new List<List<int[]>>();
            connectableIslands = GetIslandsFromPos(objectMap, randomPos[0], randomPos[1]);

            // checks if corridor can be made to island
            foreach (List<int[]> island in connectableIslands)
            {
                int[] direction = FindIslandFromPos(objectMap, randomPos[0], randomPos[1], island);
                if (direction != null && CheckCorridorToIsland(island, randomPos[0], randomPos[1], direction, false))
                {
                    tempIsles.Add(island);
                }
            }

            if (tempIsles.Count > 1)
            {
                connectableIslands = tempIsles;
                corridorCheck = true;
            }
            attempts++;
        }

        return CreateCorridorAtPoint(randomPos[0], randomPos[1]);
    }


    /// <summary>
    /// Creates a random set of corridors from point to islands within range returns islands connected
    /// </summary>
    /// <param name="row"></param>
    /// <param name="col"></param>
    private List<List<int[]>> CreateCorridorAtPoint(int row, int col)
    {
        List<List<int[]>> connectedIslands = new List<List<int[]>>();
        List<List<int[]>> isles = GetIslandsFromPos(objectMap, row, col);
        // CreateCorridorBlock(row, col);
        bool nice = true;
        // Attempts connections
        while (isles.Count > 0 && nice)
        {
            int randIndex = Random.Range(0, isles.Count - 1);
            List<int[]> randIsle = isles[randIndex];
            int[] direction = FindIslandFromPos(objectMap, row, col, randIsle);
            if (direction != null && CheckCorridorToIsland(randIsle, row, col, direction, true))
            {
                CreateCorridorToIsland(randIsle, row, col, direction);
                connectedIslands.Add(isles[randIndex]);
                isles.RemoveAt(randIndex);
            }
            else
            {
                isles.RemoveAt(randIndex);
            }
        }
        return connectedIslands;
    }

    #endregion Corridor Creation Code

    #region Island Helpers

    /// <summary>
    /// Gets the world point and returns the nearest index
    /// </summary>
    /// <param name="pos"></param>
    /// <returns></returns>
    private int[] GetWorldToIndex(Vector2 pos)
    {
        return new int[] { (int)(pos.x / roomTile.transform.localScale.x), (int)(pos.y / roomTile.transform.localScale.y) };
    }

    /// <summary>
    /// Counts the number of squares there are on the map
    /// </summary>
    /// <returns></returns>
    private int CountArea()
    {
        int numSquares = 0;
        foreach (List<int[]> island in islands)
        {
            numSquares += island.Count;
        }
        return numSquares;
    }


    /// <summary>
    /// returns the average position of the given island
    /// </summary>
    /// <returns></returns>
    private int[] AverageIslandPos(List<int[]> island)
    {
        int avgX = 0;
        int avgY = 0;
        foreach (int[] coord in island)
        {
            avgX += coord[0];
            avgY += coord[1];
        }

        avgX = avgX / island.Count;
        avgY = avgY / island.Count;
        return new int[] { avgX, avgY };
    }

    /// <summary>
    /// Finds the nearest room from center point in a stright line
    /// </summary>
    /// <param name="M"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    private int[] FindNearestRoomFromPos(GameObject[,] M, int x, int y)
    {
        int[] room = null;

        for (int i = 0; i <= 10; i++)
        {
            if (InRange(x + i, y) && M[x + i, y] != null)
            {
                room = new int[] { x + i, y };
            }
            else if (InRange(x, y + i) && M[x, y + i] != null)
            {
                room = new int[] { x, y + i };
            }
            else if (InRange(x - i, y) && M[x - i, y] != null)
            {
                room = new int[] { x - i, y };
            }
            else if (InRange(x, y - i) && M[x, y - i] != null)
            {
                room = new int[] { x, y - i };
            }

            if (room != null)
            {
                break;
            }
        }
        return room;
    }


    /// <summary>
    /// Gets all islands in a straight line from position
    /// </summary>
    /// <param name="M"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    private List<List<int[]>> GetIslandsFromPos(GameObject[,] M, int x, int y)
    {
        List<List<int[]>> isles = new List<List<int[]>>();

        bool[] directions = new bool[] { false, false, false, false };

        for (int i = 0; i <= maxCorridorLength; i++)
        {
            if (InRange(x + i, y) && M[x + i, y] != null && !directions[0])
            {
                directions[0] = true;

                if (!isles.Contains(GetIslandFromPos(x + i, y)))
                    isles.Add(GetIslandFromPos(x + i, y));
            }
            else if (InRange(x, y + i) && M[x, y + i] != null && !directions[1])
            {
                directions[1] = true;
                if (!isles.Contains(GetIslandFromPos(x, y + i)))
                    isles.Add(GetIslandFromPos(x, y + i));
            }
            else if (InRange(x - i, y) && M[x - i, y] != null && !directions[2])
            {
                directions[2] = true;
                if (!isles.Contains(GetIslandFromPos(x - i, y)))
                    isles.Add(GetIslandFromPos(x - i, y));
            }
            else if (InRange(x, y - i) && M[x, y - i] != null && !directions[3])
            {
                directions[3] = true;
                if (!isles.Contains(GetIslandFromPos(x, y - i)))
                    isles.Add(GetIslandFromPos(x, y - i));
            }
        }

        if (isles.Count > 1)
        {

        }
        return isles;
    }



    /// <summary>
    /// Finds if the given island can be seen from center point in a stright line and returns direction 
    /// </summary>
    /// <param name="M"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    private int[] FindIslandFromPos(GameObject[,] M, int x, int y, List<int[]> island)
    {
        int[] room = null;

        for (int i = 0; i < maxCorridorLength; i++)
        {
            if (InRange(x + i, y) && InIsland(island, x + i, y))
            {
                return new int[] { 1, 0 };
            }
            else if (InRange(x, y + i) && InIsland(island, x, y + i))
            {
                return new int[] { 0, 1 };
            }
            else if (InRange(x - i, y) && InIsland(island, x - i, y))
            {
                return new int[] { -1, 0 };
            }
            else if (InRange(x, y - i) && InIsland(island, x, y - i))
            {
                return new int[] { 0, -1 };
            }
        }
        return room;
    }


    /// <summary>
    /// Draws a line between the average points of two islands
    /// </summary>
    /// <param name="firstIsland"></param>
    /// <param name="secondIsland"></param>
    private void ConnectIslandsNodes(Vector3 firstIslandNode, Vector3 secondIslandNode)
    {
        LineRenderer line = NewLine();
        List<Vector3> linePositions = new List<Vector3>();

        linePositions.Add(firstIslandNode);
        linePositions.Add(new Vector3(firstIslandNode.x, secondIslandNode.y, 0));
        linePositions.Add(secondIslandNode);
        linePositions.Add(new Vector3(secondIslandNode.x, firstIslandNode.y, 0));
        linePositions.Add(firstIslandNode);

        line.positionCount = 5;
        line.SetPositions(linePositions.ToArray());
    }

    /// <summary>
    /// Given a map point draws a polygon at point
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="line"></param>
    private void DrawMarkerAtPoint(int x, int y)
    {
        Vector3 pos = new Vector3(x * roomTile.transform.localScale.x, y * roomTile.transform.localScale.y);
        DrawPolygon(5, 10, pos, 2, 2, NewLine());
    }

    /// <summary>
    /// Draws a polygon line at position
    /// </summary>
    /// <param name="vertexNumber"></param>
    /// <param name="radius"></param>
    /// <param name="centerPos"></param>
    /// <param name="startWidth"></param>
    /// <param name="endWidth"></param>
    /// <param name="lineRenderer"></param>
    private void DrawPolygon(int vertexNumber, float radius, Vector3 centerPos, float startWidth, float endWidth, LineRenderer lineRenderer)
    {
        lineRenderer.startWidth = startWidth;
        lineRenderer.endWidth = endWidth;
        lineRenderer.loop = true;
        float angle = 2 * Mathf.PI / vertexNumber;
        lineRenderer.positionCount = vertexNumber;

        for (int i = 0; i < vertexNumber; i++)
        {
            Matrix4x4 rotationMatrix = new Matrix4x4(new Vector4(Mathf.Cos(angle * i), Mathf.Sin(angle * i), 0, 0),
                                                     new Vector4(-1 * Mathf.Sin(angle * i), Mathf.Cos(angle * i), 0, 0),
                                       new Vector4(0, 0, 1, 0),
                                       new Vector4(0, 0, 0, 1));
            Vector3 initialRelativePosition = new Vector3(0, radius, 0);
            lineRenderer.SetPosition(i, centerPos + rotationMatrix.MultiplyPoint(initialRelativePosition));

        }
    }

    /// <summary>
    /// Creates and returns a new line renderer
    /// </summary>
    /// <returns></returns>
    private LineRenderer NewLine()
    {
        GameObject line = new GameObject();
        line.AddComponent<LineRenderer>();
        LineRenderer lineRenderer = line.GetComponent<LineRenderer>();
        return lineRenderer;
    }

    /// <summary>
    /// Checks if row and col are within the given island
    /// </summary>
    /// <param name="island"></param>
    /// <param name="row"></param>
    /// <param name="col"></param>
    /// <returns></returns>
    private bool InIsland(List<int[]> island, int row, int col)
    {
        foreach (int[] coord in island)
        {
            if (coord[0] == row && coord[1] == col)
            {
                return true;
            }
        }
        return false;
    }



    /// <summary>
    /// Revisit and sqaure off around given row and col
    /// </summary>
    /// <param name="M"></param>
    /// <param name="row"></param>
    /// <param name="col"></param>
    /// <param name="visited"></param>
    private int CountNeighbours(GameObject[,] M, int row, int col, bool countBorders)
    {
        int[] rowNbr = new int[] { -1, -1, -1, 0,
                                   0, 1, 1, 1 };
        int[] colNbr = new int[] { -1, 0, 1, -1,
                                   1, -1, 0, 1 };
        int numNeigbours = 0;

        for (int i = 0; i < 8; i++)
        {
            bool inRange = InRange((row + rowNbr[i]), (col + colNbr[i]));
            if (!inRange && countBorders)
            {
                numNeigbours++;
            }
            else if (inRange && M[(row + rowNbr[i]), (col + colNbr[i])] != null)
            {
                numNeigbours++;
            }

        }
        return numNeigbours;
    }



    /// <summary>
    /// Gets the neighbours of given row
    /// </summary>
    /// <param name="M"></param>
    /// <param name="row"></param>
    /// <param name="col"></param>
    /// <param name="visited"></param>
    private List<int[]> GetNeighbours(GameObject[,] M, int row, int col)
    {
        int[] rowNbr = new int[] { -1, -1, -1, 0,
                                   0, 1, 1, 1 };
        int[] colNbr = new int[] { -1, 0, 1, -1,
                                   1, -1, 0, 1 };
        List<int[]> neigbours = new List<int[]>();

        for (int i = 0; i < 8; i++)
        {
            bool inRange = InRange((row + rowNbr[i]), (col + colNbr[i]));
            if (inRange && M[(row + rowNbr[i]), (col + colNbr[i])] != null)
            {
                neigbours.Add(new int[] { (row + rowNbr[i]), (col + colNbr[i]) });
            }

        }
        return neigbours;
    }


    /// <summary>
    /// Gets the neighbours of given row
    /// </summary>
    /// <param name="M"></param>
    /// <param name="row"></param>
    /// <param name="col"></param>
    /// <param name="visited"></param>
    private List<int[]> GetNeighboursOfType(GameObject[,] M, int row, int col, RoomTile.eRoomType type)
    {
        int[] rowNbr = new int[] { -1, -1, -1, 0,
                                   0, 1, 1, 1 };
        int[] colNbr = new int[] { -1, 0, 1, -1,
                                   1, -1, 0, 1 };
        List<int[]> neigbours = new List<int[]>();

        for (int i = 0; i < 8; i++)
        {
            bool inRange = InRange((row + rowNbr[i]), (col + colNbr[i]));
            if (inRange && M[(row + rowNbr[i]), (col + colNbr[i])] != null && M[(row + rowNbr[i]), (col + colNbr[i])].GetComponent<RoomTile>().roomType == type) ;
            {
                neigbours.Add(new int[] { (row + rowNbr[i]), (col + colNbr[i]) });
            }

        }
        return neigbours;
    }

    /// <summary>
    /// Returns the island if the row and col are in the island
    /// </summary>
    /// <param name="row"></param>
    /// <param name="col"></param>
    /// <returns></returns>
    private List<int[]> GetIslandFromPos(int row, int col)
    {
        foreach (List<int[]> island in islands)
        {
            if (InIsland(island, row, col))
            {
                return island;
            }
        }
        return new List<int[]>();
    }


    /// <summary>
    /// Creates a room tile at position with given types
    /// </summary>
    /// <param name="tileType"></param>
    /// <param name="roomType"></param>
    /// <param name="row"></param>
    /// <param name="col"></param>
    /// <returns></returns>
    public GameObject CreateTileAtPos(RoomTile.eRoomType roomType, int row, int col)
    {
        GameObject obj = Instantiate(roomTile);
        RoomTile tileScript = obj.GetComponent<RoomTile>();
        obj.transform.position = new Vector2((row) * obj.transform.localScale.x, (col) * obj.transform.localScale.y);
        objectMap[row, col] = obj;
        obj.transform.parent = this.transform;
        tileScript.roomType = roomType;
        return obj;
    }


    /// <summary>
    /// Sets all tile types
    /// </summary>
    public void SetAllTileTypes()
    {
        for (int row = 0; row < objectMap.GetLength(0); row++)
        {
            for (int col = 0; col < objectMap.GetLength(1); col++)
            {
                if (objectMap[row, col] != null)
                {
                    RoomTile.eTileType tileType = RoomTile.eTileType.OpenTile;
                    RoomTile tile = objectMap[row, col].GetComponent<RoomTile>();

                    if (tile.roomType == RoomTile.eRoomType.Hall)
                    {

                    } 
                    else if (tile.roomType == RoomTile.eRoomType.Default)
                    {

                    }
                }
            }
        }
    }


    /// <summary>
    /// Sets the given positions tiles tile type based on given neighbours
    /// </summary>
    /// <param name="row"></param>
    /// <param name="col"></param>
    public void SetTileType(int row, int col, RoomTile.eTileType type)
    {
        GameObject tile = objectMap[row, col];
        RoomTile roomTile = tile.GetComponent<RoomTile>();
        roomTile.SetTileType(type);
    }

    #endregion Island Helpers
}