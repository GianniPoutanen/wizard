﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WonderingEnemy : MonoBehaviour
{
    // Movement Variables
    private Vector2 prevPos;
    public float distanceTravelled;
    private Vector3 direction = new Vector3();
    // Wondering Variables
    private bool newWonderingDirection = true;
    private float wonderingWaitTime = 1;
    private float wonderingWaitTimer;
    public float wonderingSpeed;
    // Stalking Variables - If not wondering then probably stalking
    public float viewRange;
    public float stalkingSpeed;
    public GameObject playerObject;
    private Vector2 investigatePosition;
    // State variable
    private GameManager gameManager;
    public bool wondering = true;
    public bool stalking = false;

    // Start is called before the first frame update
    void Start()
    {
        prevPos = this.transform.position;
        wonderingWaitTimer = Time.time;
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        playerObject = GameObject.Find(gameManager.PlayerCharacter);
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.y);
        if (!gameManager.Paused)
        {
            if (wondering)
            {
                Wondering();
            }
            bool playerInRange = CheckPlayerInRange();
            if (playerInRange)
            {
                investigatePosition = new Vector2(playerObject.transform.position.x, playerObject.transform.position.y);
                wondering = false;
                stalking = true;
            }
            if (stalking)
            {
                Stalking();
                Debug.Log(distanceTravelled);
                if (playerInRange)
                {
                    distanceTravelled = 1;
                }
                else
                {
                    if (distanceTravelled < 0)
                    {
                        stalking = false;
                        wondering = true;
                    }
                }
            }
        }
    }

    /// <summary>
    /// Wondering around randomly
    /// </summary>
    public void Stalking()
    {
        this.transform.position = Vector2.MoveTowards(this.transform.position, investigatePosition, stalkingSpeed);
        distanceTravelled -= Vector2.Distance(this.transform.position, prevPos);
        prevPos = this.transform.position;
        //Check in range of attack
    }

    /// <summary>
    /// Wondering around randomly
    /// </summary>
    public void Wondering()
    {
        if (newWonderingDirection)
        {
            if (wonderingWaitTimer < Time.time)
            {
                // save the current rotation
                Quaternion rotation = transform.rotation;
                // Spin it around a random amount
                transform.Rotate(Vector3.forward, Random.Range(0, 360));
                // move it a random amount forward based on it current facing
                direction = transform.right;
                // put it back to its original rotation
                transform.rotation = rotation;
                prevPos = this.transform.position;
                newWonderingDirection = false;
                distanceTravelled = Random.Range(0.2f, 1f);
            }
        }
        if (!newWonderingDirection)
        {
            // Wondering now
            if (distanceTravelled > 0)
            {
                this.transform.position += direction * wonderingSpeed;
                distanceTravelled -= Vector2.Distance(this.transform.position, prevPos);
                prevPos = this.transform.position;
            }
            else
            {
                wonderingWaitTimer = Time.time + wonderingWaitTime;
                newWonderingDirection = true;
            }
        }
    }

    private bool CheckPlayerInRange()
    {
        return 2 > Vector2.Distance(playerObject.transform.position, this.transform.position);
    }
    private Quaternion GetRotationTowardsMouse()
    {
        Vector3 direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - this.transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        return Quaternion.AngleAxis(angle, Vector3.forward);
    }
    private Quaternion GetRotationTowardsPlayer()
    {
        Vector3 direction = playerObject.transform.position - this.transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        return Quaternion.AngleAxis(angle, Vector3.forward);
    }
}
