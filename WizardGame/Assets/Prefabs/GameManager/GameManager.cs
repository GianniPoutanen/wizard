﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GameManager : MonoBehaviour
{
    public GameObject Inventory;
    public string PlayerCharacter { get; set; } = "PlayerCharacter"; // String defining the player character
    public bool Paused;
    // Start is called before the first frame update
    void Start()
    {
        Paused = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            string underMouse = "";
            foreach (RaycastResult result in RaycastPoint(Input.mousePosition))
            {
                underMouse = underMouse + result.gameObject.name +"\n";
            }
            Debug.Log(underMouse);
        }

        if (Input.GetKeyDown(KeyCode.P) &&  !(Paused && Input.GetMouseButton(0)))
        {
            Paused = !Paused;
            Inventory.SetActive(Paused);
        }   
    }


    /// <summary>
    ///  Gets all raycast results of objects under mouse
    /// </summary>
    private List<RaycastResult> RaycastPoint(Vector2 position)
    {
        PointerEventData pointerData = new PointerEventData(EventSystem.current)
        {
            pointerId = -1,
        };
        pointerData.position = position;
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointerData, results);
        return results;
    }

}
