﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileScript : MonoBehaviour
{
    public SpellSegment currentSegment;
    private float deathTimer;
    private float totalDistance;
    private Vector3 lastPosition;
    // Start is called before the first frame update
    void Start()
    {
        lastPosition = this.transform.position;
        currentSegment.SpellTransitionEffect(this.gameObject);
        this.GetComponent<Animator>().Play("FireBallAnimation", -1,  Random.Range(0.0f,1.0f));
        this.transform.localScale = new Vector3(currentSegment.spellStats.size, currentSegment.spellStats.size, currentSegment.spellStats.size);
        this.GetComponent<Animator>().speed = currentSegment.spellStats.speed;
    }

    // Update is called once per frame
    void Update()
    {
        currentSegment.SpellEffectStep();

        if (totalDistance > currentSegment.spellStats.range)
        {
            currentSegment = currentSegment.SpellFinisher();
            if (currentSegment == null)
            {
                Destroy(this.gameObject);
                return;
            }
            totalDistance = 0;
        }
        transform.position += transform.right * Time.deltaTime * currentSegment.spellStats.speed;

        // update distance travelled
        float distance = Vector3.Distance(lastPosition, transform.position);
        totalDistance += distance;
        lastPosition = transform.position;
    }

    private void OnBecameInvisible()
    {
        Destroy(this.gameObject);
    }
}
