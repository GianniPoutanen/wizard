﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class SpellSegment : MonoBehaviour
{

    [Header("Segment specific stats")]
    public string title = "";
    public SpellStats spellStats = new SpellStats();
    public int rangeRatio = 1;

    [Header("Variables for the start of spell")]
    public Vector3 spellStartPosition;
    public Vector3 spellTargetPosition;

    [Header("Changes in stats for entire spell")]
    public float accuracyChange = 0;
    public float sizeChange = 0;
    public float rangeChange = 0.1f;
    public float damageChange = 0.25f;
    public float speedChange = 0;
    public float chargeTimeChange = 0.2f;

    [Header("Changes in stats for transition in spell")]
    public float transitionDirectionChange = 0;
    public float transitionAccuracyChange = 0;
    public float transitionSizeChange = 0;
    public float transitionRangeChange = 0;
    public float transitionDamageChange = 0;
    public float transitionSpeedChange = 0;

    [Header("Following segments in the spell")]
    public List<SpellSegment> followingSegments = new List<SpellSegment>();

    [Header("Straight spell stats")]
    public int numProjectiles = 1; // number of projectiles fired
    public float projectileDelay = 0.2f; // time between projectile fire
    public float reloadTime = 0.5f;
    public int ammo = 0;

    [Header("Start spell variables")]
    public bool chargeRelease = false;
    public bool singleFire = true;
    public bool starter = false;

    public enum KeyWord
    {
        Straight,
        Split,
        Spray,
        Explode,
        Return,
        Wizzing,
        Burst,
        Far,
        DelayedSpeedUp,
    }

    public List<KeyWord> segmentKeyWords = new List<KeyWord>();

    // casts the spell at spell start position
    public void CastSpell()
    {
        switch (this.castType)
        {
            case KeyWord.Spray:
                return;
            default:
                CastProjectile();
                return;
        }
    }

    [HideInInspector]
    public bool prefabChange;
    [HideInInspector]
    public KeyWord castType;
    public GameObject changedPrefab;

    #region During Spell
    public void SpellTransitionEffect(GameObject gameObject)
    {
        // called at spell segment start
        gameObject.transform.rotation *= Quaternion.Euler(0, 0, Random.Range(-transitionDirectionChange, transitionDirectionChange));
    }
    public void SpellEffectStep()
    {
        // called in projectile update
    }

    /// <summary>
    ///  Returns spell segment if new segment doesn't destyor, else null
    /// </summary>
    /// <returns></returns>
    public SpellSegment SpellFinisher()
    {
        if (followingSegments.Count == 1)
        {
            followingSegments[0].spellStats = this.spellStats;
            return followingSegments[0];
        }
        else if (followingSegments.Count > 1)
        {
            // create a split
        }
        return null;
    }

    /// <summary>
    /// Called when scale increases
    /// </summary>
    /// <param name="gameObject"></param>    
    public void ScaleIncreaseOverTime(GameObject gameObject)
    {
        Vector3 initScale = gameObject.transform.localScale;
        gameObject.transform.localScale = new Vector3(initScale.x + sizeChange, initScale.y + sizeChange, initScale.z);
    }
    #endregion

    /// <summary>
    /// Casts a projectile spell
    /// </summary>
    public void CastProjectile()
    {
        GameObject castProjectile = GameObject.Instantiate(GetProjectileType());
        castProjectile.transform.position = this.spellStartPosition;
        castProjectile.transform.rotation = GetRotationTowardsMouse();
        castProjectile.transform.Rotate(0, 0, Random.Range(-(this.spellStats.accuracy - this.accuracyChange), (this.spellStats.accuracy - this.accuracyChange)));
        castProjectile.GetComponent<ProjectileScript>().currentSegment = this;
    }

    /// <summary>
    /// Finds the projectile in the assets library
    /// </summary>
    /// <returns></returns>
    private GameObject GetProjectileType()
    {
        return AssetDatabase.LoadAssetAtPath<UnityEngine.GameObject>("Assets/Prefabs/Spells/ProjectileFactory/TestProjectile.prefab");
    }

    /// <summary>
    /// Gets the rotational angle from this object towards the mouse
    /// </summary>
    /// <returns></returns>
    private Quaternion GetRotationTowardsMouse()
    {
        Vector3 direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - this.spellStartPosition;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        return Quaternion.AngleAxis(angle, Vector3.forward);
    }

    /// <summary>
    /// Gets the angle between point a and b
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns></returns>
    private float AngleBetweenTwoPoints(Vector3 a, Vector3 b)
    {
        return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
    }

    public void UpdateSpellStats()
    {
        spellStats = new SpellStats();
        List<SpellSegment> segments = new List<SpellSegment>();
        segments.Add(this);
        while (segments.Count > 0)
        {
            foreach (SpellSegment followingSegment in segments[0].followingSegments)
            {
                segments.Add(followingSegment);
            }
            spellStats.range += segments[0].rangeChange;
            spellStats.damage += segments[0].damageChange;
            spellStats.accuracy += segments[0].accuracyChange;
            spellStats.size += segments[0].sizeChange;
            spellStats.speed += segments[0].speedChange;
            spellStats.chargeTime += segments[0].chargeTimeChange;
            spellStats.ammo += segments[0].ammo;
            spellStats.reloadTime += segments[0].reloadTime;
            segments.RemoveAt(0);
        }

        // default the neg values to lowest values 
        if (spellStats.range < 0.15f) spellStats.range = 0.15f;
        if (spellStats.damage < 0.1f) spellStats.damage = 0.1f;
        if (spellStats.accuracy < 0) spellStats.accuracy = 0;
        if (spellStats.size < 0.1f) spellStats.size = 0.1f;
        if (spellStats.speed < 0.4f) spellStats.speed = 0.3f;
        if (spellStats.chargeTime < 0.02f) spellStats.chargeTime = 0.02f;

        Debug.Log("Spell updated! - Dmg: " + spellStats.damage + "  Range: " + spellStats.range +
                                "  Accuracy: " + spellStats.accuracy + "  Speed: " + spellStats.speed +
                                "  Size: " + spellStats.size + "  Charge Time: " + spellStats.chargeTime +
                                "  Ammo: " + spellStats.ammo + "  Reload Time: " + spellStats.reloadTime);
    }
}
